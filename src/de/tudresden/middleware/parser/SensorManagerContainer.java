package de.tudresden.middleware.parser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.tudresden.middleware.businessobjects.manager.Sensor;

// TODO: The name of this class is not so descriptive. Change to something better.
public class SensorManagerContainer {

    private class Index {

        private String type;
        private String manufacturer;
        private String model;

        public Index(String type, String manufacturer, String model) {
            this.type = type;
            this.manufacturer = manufacturer;
            this.model = model;
        }

        public String getManufacturer() {
            return manufacturer;
        }

        public String getType() {
            return type;
        }

        public String getModel() {
            return model;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Index index = (Index) o;

            if (!type.equals(index.type)) return false;
            if (!manufacturer.equals(index.manufacturer)) return false;
            return model.equals(index.model);

        }

        @Override
        public int hashCode() {
            int result = type.hashCode();
            result = 31 * result + manufacturer.hashCode();
            result = 31 * result + model.hashCode();
            return result;
        }

        @Override
        public String toString() {
            return "Index{" +
                    "type='" + type + '\'' +
                    ", manufacturer='" + manufacturer + '\'' +
                    ", model='" + model + '\'' +
                    '}';
        }
    }

    private Map<Index, Sensor> sensorMap = null;

    public SensorManagerContainer(List<Sensor> sensors) {
        if(sensors == null) {
            throw new NullPointerException("Sensors should not be null");
        }

        sensorMap = new HashMap<Index, Sensor>();

        for(Sensor sensor : sensors) {
            Index index = new Index(sensor.getSensorType().getName(), sensor.getManufacturer(), sensor.getModel());
            sensorMap.put(index, sensor);
        }

    }

    public Sensor getSensorProperty(String type, String manufacturer, String model) {
        Index index = new Index(type, manufacturer, model);
        Sensor sensor = sensorMap.get(index);
        return sensor;
    }

    public void printDataStructure() {
        final Set<Index> indexes = sensorMap.keySet();

        for(Index index : indexes) {
            System.out.println("index = " + index);
            System.out.println(sensorMap.get(index));
        }

    }
}
