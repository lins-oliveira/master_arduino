package de.tudresden.middleware.sandbox;

import gnu.io.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class SerialConnector {

    private static final Logger LOGGER = LogManager.getLogger(SerialConnector.class.getName());

    private final static int TIMEOUT = 2000;
    private final static int DATA_RATE = 9600;

    private static BlockingQueue<String> inputQueue = new ArrayBlockingQueue<String>(1000);
    private static BlockingQueue<String> outputQueue = new ArrayBlockingQueue<String>(1000);

    public void connect(String portName) throws Exception {
        LOGGER.trace("initializing SerialConnector");
        CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(portName);

        if (portIdentifier.isCurrentlyOwned()) {
            LOGGER.error("Port is currently in use");
        } else {
            CommPort commPort = portIdentifier.open(this.getClass().getName(), TIMEOUT);

            if (commPort instanceof SerialPort) {
                SerialPort serialPort = (SerialPort) commPort;
                serialPort.setSerialPortParams(DATA_RATE, SerialPort.DATABITS_8,
                        SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

                InputStream in = serialPort.getInputStream();
                OutputStream out = serialPort.getOutputStream();

                LOGGER.trace("Spanning SerialReader thread...");
                Thread tin = new Thread(new SerialReader(in));
                LOGGER.trace("Spanning SerialWriter thread...");
                Thread tout = new Thread(new SerialWriter(out));
                LOGGER.trace("Spanning DataProcessor thread...");
                Thread processor = new Thread(new DataProcessor(out));

                tin.start();
                tout.start();
                processor.start();

            } else {
                LOGGER.error("Only serial ports are handled by this example.");
            }
        }
    }

    public static class DataProcessor implements Runnable {

        OutputStream out;

        public DataProcessor(OutputStream out) {
            this.out = out;
        }

        @Override
        public void run() {
            while (true) {
                if (!inputQueue.isEmpty()) {
                    LOGGER.trace("Processing...");
                    LOGGER.trace("queue lenght: " + inputQueue.toArray().length);
                    List<String> elements = new ArrayList<String>();
                    int length = inputQueue.drainTo(elements);
                    LOGGER.trace("Drained lines: " + length);

                    if (length > 0) {
                        try {
                            outputQueue.put("Caralhooooo!");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    LOGGER.trace("Nothing to process...");
                }
                // Wait for 1000 milisseconds.
                try {
                    LOGGER.trace("Going to sleep...");
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public static class SerialReader implements Runnable {

        InputStream in;

        public SerialReader(InputStream in) {
            this.in = in;
        }

        public void run() {
            byte[] buffer = new byte[1024];
            int data = -1;

            try {
                LOGGER.trace("Reading lines...");
                int len = 0;
                int lines = 1;
                while ((data = this.in.read()) > -1) {
                    if (data == '\n') {
                        String line = new String(buffer, 0, len);
                        LOGGER.trace("Lines read: " + lines++);
                        LOGGER.trace(line.replace("\n", "").replace("\r", ""));
                        try {
                            inputQueue.put(line.replace("\n", "").replace("\r", ""));
                        } catch (InterruptedException e) {
                            // Happens when queue is full, can use add instead if you dont want to wait for space.
                            e.printStackTrace();
                        }
                        LOGGER.trace("Queue size:" + inputQueue.size());
                        len = 0;
                        buffer = new byte[1024];
                    }
                    buffer[len++] = (byte) data;
                }
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }

    }

    public static class SerialWriter implements Runnable {

        OutputStream out;

        public SerialWriter(OutputStream out) {
            this.out = out;
        }

        public void run() {
            try {
                while (true) {
                    if (outputQueue.size() > 0) {
                        LOGGER.trace("Preparing to write...");
                        List<String> messages = new ArrayList<String>();
                        outputQueue.drainTo(messages);

                        for (String message : messages) {
                            LOGGER.trace("Writing message: " + message);

                            this.out.write(message.getBytes(), 0, message.length());
                            LOGGER.trace("Finish to write message.");
                        }
                    }
                    try {
                        LOGGER.trace("Going to sleep...");
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        LOGGER.catching(e);
                    }
                }
            } catch (IOException e) {
                LOGGER.catching(e);
            }
        }
    }
}