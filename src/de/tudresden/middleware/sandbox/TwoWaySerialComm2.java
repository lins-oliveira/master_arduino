package de.tudresden.middleware.sandbox;

import gnu.io.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class TwoWaySerialComm2
{

    static void threadMessage(String message) {
        String threadName = Thread.currentThread().getName();
        System.out.format("%s: %s%n", threadName, message);
    }
    
    void connect(String portName) throws Exception
    {
        threadMessage("Connecting...");
        System.setProperty("gnu.io.rxtx.SerialPorts", portName);

        CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(portName);
        if (portIdentifier.isCurrentlyOwned()) {
            System.out.println("Error: Port is currently in use");
        } else {
            CommPort commPort = portIdentifier.open(this.getClass().getName(), 2000);
            
            if (commPort instanceof SerialPort) {
                threadMessage("Creating serial port instance...");
                SerialPort serialPort = (SerialPort) commPort;
                serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

                threadMessage("Creating input stream...");
                InputStream in = serialPort.getInputStream();

                threadMessage("Creating output stream...");
                OutputStream out = serialPort.getOutputStream();
                               
                Thread tout = new Thread(new SerialWriter(out));
                tout.start();
                
//                serialPort.addEventListener(new SerialReader(in, this.messagePool));
//                serialPort.notifyOnDataAvailable(true);

                int counter = 0;

                while (tout.isAlive()) {
                    threadMessage("---------------------------------->Still waiting...");
                    // Wait maximum of 1 second
                    // for MessageLoop thread
                    // to finish.
                    out.write("Caralho".getBytes(), 0, "Caralho".length());
//                    tout.join(1000);
                    Thread.sleep(1000);
                    counter++;

                    if (counter == 10) {
                        tout.interrupt();
                        tout.join();
                    }
                }

            } else {
                System.out.println("Error: Only serial ports are handled by this example.");
            }
        }     
    }
    
    /**
     * Handles the input coming from the serial port. A new line character
     * is treated as the end of a block in this example. 
     */
    public static class SerialReader implements SerialPortEventListener {
        private InputStream in;
        private byte[] buffer = new byte[1024];
        
        public void serialEvent(SerialPortEvent arg0) {
            threadMessage("New serial port event...");
            int data;
          
            try {
                int len = 0;
                while ((data = in.read()) > -1) {
                    if (data == '\n') {
                        break;
                    }
                    buffer[len++] = (byte) data;
                }
                System.out.println(new String(buffer, 0, len));

            }
            catch (IOException e) {
                e.printStackTrace();
                System.exit(-1);
            }             
        }
    }

    /** */
    public static class SerialWriter implements Runnable {
        OutputStream out;
        
        public SerialWriter(OutputStream out) {
            this.out = out;
        }
        
        public void run() {
            threadMessage("Serial write...");
            try  {
                threadMessage("try...");
                int c = 0;
                while ((c = System.in.read()) > -1) {
                    System.out.println(c);
                    this.out.write(c);
                }
            }
            catch(IOException e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }
    }

    public static void main(String[] args) {

        try {
            (new TwoWaySerialComm2()).connect("/dev/ttyACM0");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}

