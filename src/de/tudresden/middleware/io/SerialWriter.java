package de.tudresden.middleware.io;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

public class SerialWriter implements Runnable {

    private static final Logger LOGGER = LogManager.getLogger(SerialWriter.class.getName());

    private OutputStream out;
    private BlockingQueue<String> outputQueue;

    public SerialWriter(OutputStream out, BlockingQueue<String> outputQueue) {
        this.out = out;
        this.outputQueue = outputQueue;
    }

    public void run() {
        try {
            while (true) {
                if (outputQueue.size() > 0) {
                    LOGGER.trace("Preparing to write...");
                    List<String> messages = new ArrayList<String>();
                    outputQueue.drainTo(messages);

                    for (String message : messages) {
                        LOGGER.trace("Writing message: " + message);

                        this.out.write(message.getBytes(), 0, message.length());
                        LOGGER.trace("Finish to write message.");
                    }
                }
                try {
                    LOGGER.trace("Going to sleep...");
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    LOGGER.catching(e);
                }
            }
        } catch (IOException e) {
            LOGGER.catching(e);
        }
    }
}