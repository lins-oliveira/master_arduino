package de.tudresden.middleware.io;

import de.tudresden.middleware.businessobjects.sampling.Sample;
import de.tudresden.middleware.parser.SamplingJsonParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

public class DataProcessor implements Runnable {

    private static final Logger LOGGER = LogManager.getLogger(DataProcessor.class.getName());

    private OutputStream out;
    private BlockingQueue<String> inputQueue;
    private BlockingQueue<String> outputQueue;


    public DataProcessor(OutputStream out, BlockingQueue<String> inputQueue, BlockingQueue<String> outputQueue) {
        this.out = out;
        this.inputQueue = inputQueue;
        this.outputQueue = outputQueue;
    }

    @Override
    public void run() {
        while (true) {
            if (!inputQueue.isEmpty()) {
                LOGGER.trace("Processing...");

                LOGGER.trace("queue lenght: " + inputQueue.toArray().length);
                List<String> elements = new ArrayList<String>();
                int length = inputQueue.drainTo(elements);
                LOGGER.trace("Drained lines: " + length);

                for(String line : elements){
                    Sample sample = SamplingJsonParser.parseLine(line);
                    LOGGER.trace(sample);

                }

                if (length > 0) {
                    try {
                        outputQueue.put("Caralhooooo!");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                LOGGER.trace("Nothing to process...");
            }
            // Wait for 1000 milisseconds.
            try {
                LOGGER.trace("Going to sleep...");
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}