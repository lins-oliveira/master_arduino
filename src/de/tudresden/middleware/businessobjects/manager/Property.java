package de.tudresden.middleware.businessobjects.manager;

public class Property {

    private enum Boundary {
        EQ("Equals"), GT("Greater than"), LT("Lower than"), DV("Standard deviation");

        private String name;

        Boundary(String name){
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    private Long id;
	private PropertyType propertyType;
	private Float value;
	private String unit;
	private Boundary boundary;

    public Property(Long id, PropertyType propertyType, Float value, String unit, Boundary boundary) {
        this.id = id;
        this.propertyType = propertyType;
        this.value = value;
        this.unit = unit;
        this.boundary = boundary;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PropertyType getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(PropertyType propertyType) {
        this.propertyType = propertyType;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Boundary getBoundary() {
        return boundary;
    }

    public void setBoundary(Boundary boundary) {
        this.boundary = boundary;
    }

    @Override
    public String toString() {
        return "Property{" +
                "id=" + id +
                ", propertyType=" + propertyType +
                ", value=" + value +
                ", unit='" + unit + '\'' +
                ", boundary=" + boundary +
                '}';
    }
}
