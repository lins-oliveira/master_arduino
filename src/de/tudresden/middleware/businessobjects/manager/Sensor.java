package de.tudresden.middleware.businessobjects.manager;

import java.util.List;

public class Sensor {

    private Long id;
	private SensorType sensorType;
	private String model;
	private String manufacturer;
	private List<Property> properties;
    private List<TimedProperty> timedProperties;

    public Sensor(Long id, SensorType sensorType, String model, String manufacturer, List<Property> properties, List<TimedProperty> timedProperties) {
        this.id = id;
        this.sensorType = sensorType;
        this.model = model;
        this.manufacturer = manufacturer;
        this.properties = properties;
        this.timedProperties = timedProperties;
    }

    public Sensor(Long id, SensorType sensorType, String model, String manufacturer, List<Property> properties) {
        this.id = id;
        this.sensorType = sensorType;
        this.model = model;
        this.manufacturer = manufacturer;
        this.properties = properties;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SensorType getSensorType() {
        return sensorType;
    }

    public void setSensorType(SensorType sensorType) {
        this.sensorType = sensorType;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public List<Property> getProperties() {
        return properties;
    }

    public void setProperties(List<Property> properties) {
        this.properties = properties;
    }

    @Override
    public String toString() {
        return "Sensor{" +
                "id=" + id +
                ", sensorType=" + sensorType +
                ", model='" + model + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", properties=" + properties +
                ", timedProperties=" + timedProperties +
                '}';
    }
}
