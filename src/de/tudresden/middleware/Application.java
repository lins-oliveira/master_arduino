package de.tudresden.middleware;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.tudresden.middleware.businessobjects.manager.Sensor;
import de.tudresden.middleware.http.APIHTTPClient;
import de.tudresden.middleware.io.DataProcessor;
import de.tudresden.middleware.io.SerialReader;
import de.tudresden.middleware.io.SerialWriter;
import de.tudresden.middleware.parser.PropertyJsonParser;
import de.tudresden.middleware.parser.SensorManagerContainer;

public class Application {

    private static final Logger LOGGER = LogManager.getLogger(Application.class.getName());

    public static final String PROPERTY_MANAGER_ENDPOINT = "http://localhost:8080/sensor/json/all";
    public static final String PORT_NAME = "/dev/ttyACM0";

    private final static int TIMEOUT = 2000;
    private final static int DATA_RATE = 9600;

    private static BlockingQueue<String> inputQueue = new ArrayBlockingQueue<String>(1000);
    private static BlockingQueue<String> outputQueue = new ArrayBlockingQueue<String>(1000);

    public static void main(String[] args) {

        LOGGER.trace("Starting application");

        LOGGER.trace("Connecting to sensor manager server");

        LOGGER.trace("Getting all sensors and its properties");

        LOGGER.trace("Creating HTTP Client");
        APIHTTPClient client = new APIHTTPClient(PROPERTY_MANAGER_ENDPOINT);
        client.connect();

        List<Sensor> sensors = null;
        try {
            LOGGER.trace("Trying to get all sensor properties");
            String result = client.getAllSensorProperties();
            sensors = PropertyJsonParser.parse(result);
            LOGGER.info("Sensor List: " + sensors);
        } catch (Exception e) {
            LOGGER.error("Error to connect sensor manager");
            LOGGER.catching(e);
        }
        SensorManagerContainer sensorManagerContainer = new SensorManagerContainer(sensors);

        // TODO: Remove those tests from here and put in a different package.
//        Sensor sensor01 = sensorManagerContainer.getSensorProperty("Thermometer", "Generic", "LM35");
//        System.out.println("sensor01 = " + sensor01);
//
//        Sensor sensor02 = sensorManagerContainer.getSensorProperty("Thermometer", "Generic", "LM35");
//        System.out.println("sensor02 = " + sensor02);

        LOGGER.trace("Creating serial port connection to arduino");

        try {
            LOGGER.trace("Creating connection...");
            System.setProperty("gnu.io.rxtx.SerialPorts", PORT_NAME);

            LOGGER.trace("initializing SerialConnector");
            CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(PORT_NAME);

            if (portIdentifier.isCurrentlyOwned()) {
                LOGGER.error("Port is currently in use");
            } else {
                CommPort commPort = portIdentifier.open(Application.class.getName(), TIMEOUT);

                if (commPort instanceof SerialPort) {
                    SerialPort serialPort = (SerialPort) commPort;
                    serialPort.setSerialPortParams(DATA_RATE, SerialPort.DATABITS_8,
                            SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

                    InputStream in = serialPort.getInputStream();
                    OutputStream out = serialPort.getOutputStream();

                    LOGGER.trace("Spanning SerialReader thread...");
                    Thread serialReader = new Thread(new SerialReader(in, inputQueue));

                    LOGGER.trace("Spanning SerialWriter thread...");
                    Thread serialWriter = new Thread(new SerialWriter(out, outputQueue));

                    LOGGER.trace("Spanning DataProcessor thread...");
                    Thread dataProcessor = new Thread(new DataProcessor(out, inputQueue, outputQueue));

                    serialReader.start();
                    serialWriter.start();
                    dataProcessor.start();

                } else {
                    LOGGER.error("Only serial ports can be handled.");
                }
            }
        } catch (PortInUseException e) {
            LOGGER.catching(e);
        } catch (Exception e) {
            LOGGER.error("It was not possible to connect with arduino.");
            LOGGER.catching(e);
        }

    }
}
